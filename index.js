// console.log('add post');

let posts = [];
// post will serve as our mock database.
// array of objects
/*
    {
        id: value,
        title: value,
        body: value
    }
*/

let count = 1;

// Add post data to the mock database
// select first ung form for add post using the id. then add ka ng envent listener na pag sinubmit sya (using 'submit' button), mangyayari ung 'event'
document.querySelector("#form-add-post").addEventListener("submit", (event) => {
    // preventDefault() functions stops the auto reload of the webpage when submitting
    // para pag click kay submit hindi mag autoreload ung page
    event.preventDefault(); // ung event dito ay yung parameter na ginamit sa addEventListener. that means ang event na nilagay after maclick ng submit eh prevent ung default which is reloading the page


    // updates ung mock database depende sa magiging input ng userr
    let newPost = {
        id: count,
        title: document.querySelector("#txt-title").value,
        body: document.querySelector("#txt-body").value
    }

    console.log(newPost)

    // para mailagay ung input ni user as update sa mock database
    posts.push(newPost);
    console.log(posts);

    count++ // para everytime na magsubmit, magbabago ung id number. which is at first may value ng 1
    showPosts(posts); // invoking ng different function sya na nilagay sa dulo neto kasi para after submit, matrigger na agad ung showPosts na function

    document.querySelector("#txt-title").value = ""
    document.querySelector("#txt-body").value = ""


})

// Show posts
const showPosts = (posts) => {
    let postEntries = ``;

    posts.reverse()
    posts.forEach((post) => {
        postEntries += `
        <div id = "post-${post.id}">
            <h3 id = "post-title-${post.id}">${post.title}</h3>
            <p id = "post-body-${post.id}">${post.body}</p>
            <button onclick="editPost(${post.id})">Edit</button>
            <button onclick="deletePost(${post.id})">Delete</button>
        </div>
        `
    })

    document.querySelector("#div-post-entries").innerHTML = postEntries
}

// Edit post
const editPost = (id) => {
    let title = document.querySelector(`#post-title-${id}`).innerHTML
    let body = document.querySelector(`#post-body-${id}`).innerHTML

    console.log(title);
    console.log(body);

    document.querySelector("#txt-edit-id").value = id;

    document.querySelector("#txt-edit-title").value = title;

    document.querySelector("#txt-edit-body").value = body;
}

document.querySelector("#form-edit-post").addEventListener("submit", (event) => {
    event.preventDefault();

    // forEach to check/find the document to be edited
    posts.forEach((post) => {
        let idToBeEdited = document.querySelector("#txt-edit-id").value
        console.log(typeof idToBeEdited);
        console.log(typeof post.id);
        // 2 == lng kasi different data type talaga so magfafalse lagi
        if (post.id == idToBeEdited) {
            let title = document.querySelector("#txt-edit-title").value
            let body = document.querySelector("#txt-edit-body").value

            posts[post.id - 1].title = title;
            posts[post.id - 1].body = body;

            alert("Edit is scucessful")
            showPosts(posts)

            document.querySelector("#txt-edit-title").value="";
            document.querySelector("#txt-edit-body").value=""
            document.querySelector("#txt-edit-id").value=""
        }

    })
})



// delete post
const deletePost = (id) => {
    const postIndex = posts.findIndex(post => {
        return post.id == id
    })
    posts.splice(postIndex, 1)
    // document.querySelector(`#post-${id}`).remove()
    showPosts(posts)
}
